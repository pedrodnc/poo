
package pruebapoo;

public class Persona {
    String nombre;
    String apellidos;
    int edad;
    float altura;
    String puesto;
    float sueldo;
    Vaca[]misVacas;
    
    public Persona(String n, String a, int e, float al, String p,float s){
        this.nombre=n;
        this.apellidos=a;
        this.edad=e;
        this.altura=al;
        this.puesto=p;
        this.sueldo=s;
        this.misVacas=new Vaca[15];
    }
    
    
    public String imprimePersona(){
        return this.nombre+" : "+this.apellidos+" : "+this.edad+" : "+this.altura;
    }
    
    public boolean esMayorDe(int edad){
        return this.edad>edad;
    }
    public String cuantoGana(){
        return "Gana"+this.sueldo+ "€";
    }
    public float proporcionSueldo(Persona per){
         return per.sueldo/this.sueldo;
    }
    public void ajustaSeldoA(Persona per){
        if(this.puesto.equals("ordeño")&&(per.puesto.equals("gerencia"))){
            this.sueldo=0.5f*per.sueldo;
        } else
        if(this.puesto.equals("ordeño")&&(per.puesto.equals("carniceria"))){
            this.sueldo=1.25f*per.sueldo;
        } else
        if(this.puesto.equals("carniceria")&&(per.puesto.equals("ordeño"))){
            this.sueldo=0.75f*per.sueldo;
        } else
        
        if(this.puesto.equals("carniceria")&&(per.puesto.equals("gerencia"))){
            this.sueldo=0.25f*per.sueldo;
        } else
        if(this.puesto.equals("gerencia")&&(per.puesto.equals("ordeño"))){
            this.sueldo=1.5f*per.sueldo;
        } else
        
        if(this.puesto.equals("gerenica")&&(per.puesto.equals("carniceria"))){
            this.sueldo=1.75f*per.sueldo;
        } else{
            this.sueldo=per.sueldo;
        }
            
    }
    
    public void quedarmeVacas(Vaca[] todas,Vaca v){
        int u=0;
        this.misVacas=new Vaca[todas.length];
        for(int i=0;i<todas.length;i++){
            if(this.puesto.equals(todas[i].funcion)){
                this.misVacas[u]=todas[i];
            }
        }
        
        
        
        if(this.puesto.equals(v.funcion)){
            for(int i=0;i<todas.length;i++){
                this.misVacas[i]=todas[i];
            }
        }
                 
        //Recorrer el array todas, si la funcion de la vaca coincide con mi puesto, meto todas[i] en this.misVacas
    }
}
/*-Añadir a la clase Persona un String "puesto". Usarlo solo para valores "ordeño","carniceria" o "gerencia".

-Añadir a la clase Persona un Float "sueldo", que expresará el sueldo en euros.

-añadir un método cuantoGana a la clase Persona, que devuelva el String "Gana"+sueldo+" €"

-añadir un método proporcionSueldo a la clase Persona, que reciba una persona por argumentos, y devuelva un 
float con cuántas veces gana esa persona el sueldo de this.

-Añadir un método ajustaSueldoA, que reciba un objeto Persona, y asigne el sueldo de this, poniéndolo en proporción al del 
objeto que recibe por parámetro. Un/a ordeñador/a debería ganar un 25% más que alguien de carnicería, y un/a gerente un 50% 
más que alguien de ordeño.

-Crear la clase Vaca, con Strings para nombre, número de serie y función ("ordeño" o "carniceria"), un Float peso, un int edad,
y un array de float con litros ordeñados en el día.

-Crear un método asignaFunción a una vaca, de forma que si pesa más de 200kg, se asigne a carnicería, y si pesa menos, a ordeño.

-Añadir a la clase persona un Array de Vacas

-Crear en la clase persona un método quedarmeVacas(Vaca[] vac), que copie en el array interno a las vacas que corresponden
a su puesto de trabajo.*/