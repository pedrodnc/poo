
package pruebapoo;

public class Vaca {
    String nombre;
    int numeroSerie;
    String funcion;
    float peso;
    int edad;
    float[]litrosOrdeñadosDia;
    
    
    public void asignaFuncion(){
        if (this.peso>=200){
            this.funcion="carniceria";
        }else this.funcion="ordeño";
    }
    /**
     * Construye el o9bjeto vaca con sus datos+
     * @param n nombre
     * @param ns numeroserie
     * @param p peso
     * @param e edad
     * @param f funcion
     */
    public Vaca(String n, int ns, float p, int e, String f ){
        this.nombre=n;
        this.numeroSerie=ns;
        this.peso=p;
        this.edad=e;
        this.funcion=f;
        this.litrosOrdeñadosDia=new float[15];
    }
    
    public Vaca(String n, int ns, float p, int e){
        this.nombre=n;
        this.numeroSerie=ns;
        this.peso=p;
        this.edad=e;
        this.litrosOrdeñadosDia=new float[15];
    }
    
}
